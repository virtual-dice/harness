import React from 'react';
import ReactDOM from 'react-dom';

import { createDie } from '@virtual-dice/3d-die';

import { App } from './components';

import './global.scss';

createDie();

ReactDOM.render(<App />, document.getElementById('root'));
