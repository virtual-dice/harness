import React from 'react';

export const mockComponent = name => props => (<div { ...props } data-mock={ name } />);
