import React from 'react';

import { shallow } from 'enzyme';

describe('Given the mockComponent helper', () => {
    let mockComponent;

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            ({ mockComponent } = require('../mockComponent'));
        });

        it('Then it should be a function', () => {
            mockComponent.should.be.a('function');
        });

        describe('And when invoked with a name', () => {
            const expectedName = 'SomeName';

            let ActualMockedComponent;

            beforeEach(() => {
                ActualMockedComponent = mockComponent(expectedName);
            });

            it('Then it should return a function', () => {
                ActualMockedComponent.should.be.a('function');
            });

            describe('And rendered as a React component', () => {
                const expectedProps = { foo: 'bar' };

                let actualSubject;

                beforeEach(() => {
                    actualSubject = shallow(<ActualMockedComponent { ...expectedProps } />);
                });

                it('Then it should be a div', () => {
                    actualSubject.should.be.of.type('div');
                });

                it('Then it should contain the data-mock with the expected name', () => {
                    actualSubject.should.have.attr('data-mock', expectedName);
                });

                it('Then it should contain the expected properties', () => {
                    actualSubject.should.have.props(expectedProps);
                });
            });
        });
    });
});
