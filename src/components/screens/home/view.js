import React from 'react';
import * as THREE from 'three';

import { Canvas } from 'react-three-fiber';

import styles from './styles.module.scss';

const Thing = ({ vertices, color }) => (
    <group ref={ () => console.log('we have access to the instance') }>
        <line>
            <geometry
                attach="geometry"
                vertices={ vertices.map(v => new THREE.Vector3(...v)) }
                onUpdate={ self => self.verticesNeedUpdate = true }
            />

            <lineBasicMaterial attach="material" color="black" />
        </line>

        <mesh
            onClick={ () => console.log('click') }
            onPointerOver={ () => console.log('hover') }
            onPointerOut={ () => console.log('unhover') }
        >
            <octahedronGeometry attach="geometry" />

            <meshBasicMaterial
                attach="material"
                color={ color }
                opacity={ 0.5 }
                transparent
            />
        </mesh>
    </group>
);

const vertices = [
    [ -1, 0, 0 ],
    [ 0, 1, 0 ],
    [ 1, 0, 0 ],
    [ 0, -1, 0 ],
    [ -1, 0, 0 ]
];

const color = 'blue';

export const HomeView = () => (
    <div className={ styles.component }>
        <Canvas width="100%" height="100%">
            <Thing vertices={ vertices } color={ color } />
        </Canvas>
    </div>
);
