import React from 'react';

import { shallow } from 'enzyme';

import { mockComponent } from '../../../../helpers';

const MockedCanvas = mockComponent('Canvas');
const mockedStyleComponent = 'some-mocked-style-component';

describe('Given the Home view', () => {
    let HomeView;

    beforeEach(() => {
        jest.doMock('react-three-fiber', () => ({
            Canvas: MockedCanvas
        }));

        jest.doMock('../styles.module.scss', () => ({
            component: mockedStyleComponent
        }));
    });

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            ({ HomeView } = require('../view'));
        });

        it('It should be a function', () => {
            HomeView.should.be.a('function');
        });

        describe('And rendered as a React Component', () => {
            let actualSubject;

            beforeEach(() => {
                actualSubject = shallow(<HomeView />);
            });

            it('It should be a div', () => {
                actualSubject.should.be.of.type('div');
            });

            it('Then it should contain the expected className', () => {
                actualSubject.should.have.className(mockedStyleComponent);
            });

            it('Then it should contain a single Canvas', () => {
                actualSubject.find(MockedCanvas).should.have.length(1);
            });

            describe('And the Canvas', () => {
                beforeEach(() => {
                    actualSubject = actualSubject.find(MockedCanvas).at(0);
                });

                it('It should have the expected width', () => {
                    actualSubject.should.have.prop('width', '100%');
                });

                it('It should have the expected height', () => {
                    actualSubject.should.have.prop('height', '100%');
                });
            });
        });
    });

    afterEach(() => {
        jest.resetModules();
    });
});
