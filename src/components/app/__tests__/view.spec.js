import React from 'react';

import { shallow } from 'enzyme';

import { mockComponent } from '../../../helpers';

const MockedBrowserRouter = mockComponent('BowserRouter');
const MockedRoute = mockComponent('Route');
const MockedHome = mockComponent('Home');

describe('Given the app view component', () => {
    let AppView;

    beforeEach(() => {
        jest.doMock('react-router-dom', () => ({
            BrowserRouter: MockedBrowserRouter,
            Route: MockedRoute
        }));

        jest.doMock('../../screens', () => ({ Home: MockedHome }));
    });

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            ({ AppView } = require('../view'));
        });

        it('Then it should be a function', () => {
            expect(AppView).toBeInstanceOf(Function);
        });

        describe('And rendered as a React component', () => {
            let actualSubject;

            beforeEach(() => {
                actualSubject = shallow(<AppView />);
            });

            it('Then it should render a BrowserRouter', () => {
                actualSubject.should.be.of.type(MockedBrowserRouter);
            });

            it('Then it should contain a single Home route', () => {
                actualSubject.find(MockedRoute)
                    .filter({ component: MockedHome })
                    .should.have.length.of(1);
            });

            describe('And the Home Route', () => {
                const expectedPath = '/';

                beforeEach(() => {
                    actualSubject = actualSubject.find(MockedRoute)
                        .filter({ component: MockedHome })
                        .at(0);
                });

                it('It should have the expected path', () => {
                    actualSubject.should.have.prop('path', expectedPath);
                });
            });
        });
    });

    afterEach(() => {
        jest.resetModules();
    });
});
