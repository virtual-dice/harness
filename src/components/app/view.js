import React from 'react';

import { BrowserRouter, Route } from 'react-router-dom';

import { Home } from '../screens';

export const AppView = () => (
    <BrowserRouter>
        <Route path="/" component={ Home } />
    </BrowserRouter>
);
