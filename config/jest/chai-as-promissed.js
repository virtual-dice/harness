import chaiAsPromised from 'chai-as-promised';

export const installChaiAsPromised = chai => {
    chai.use(chaiAsPromised);
};
