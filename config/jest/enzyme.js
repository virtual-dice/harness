import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import chaiEnzyme from 'chai-enzyme';

export const installEnzyme = chai => {
    const configuration = {
        adapter: new Adapter()
    };

    configure(configuration);

    chai.use(chaiEnzyme());
};
